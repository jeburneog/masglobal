package com.masglobal.controller;


import com.masglobal.model.Employee;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.function.Function;


@RestController
@RequestMapping("/api")
public class EmployeeController {

    private static final String API_EMPLOYEES = "http://masglobaltestapi.azurewebsites.net/api/Employees";

    private final WebClient webClient;

    public EmployeeController(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl(API_EMPLOYEES).build();
    }


    @GetMapping
    public Flux<Employee> getEmployees() {

        return this.webClient
                .get()
                .retrieve()
                .bodyToFlux(Employee.class)
                .map(employeeEmployeeFunction);
    }

    @GetMapping("/{id}")
    public Flux<Employee> getEmployeesById(@PathVariable Integer id) {

        return this.webClient
                .get()
                .retrieve()
                .bodyToFlux(Employee.class)
                .filter(employee -> employee.getId().equals(id))
                .map(employeeEmployeeFunction);
    }

    private final Function<Employee, Employee> employeeEmployeeFunction = employee -> new Employee(
            employee.getId(),
            employee.getName(),
            employee.getContractTypeName(),
            employee.getRoleId(),
            employee.getRoleName(),
            employee.getRoleDescription(),
            employee.getHourlySalary(),
            employee.getMonthlySalary());
    
}
