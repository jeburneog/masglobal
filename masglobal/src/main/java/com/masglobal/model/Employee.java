package com.masglobal.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Employee {

    private static final int AGE_VALUE = 12;
    private static final int VALUE = 120;

    private Integer id;
    private String name;
    private String contractTypeName;
    private Integer roleId;
    private String roleName;
    private String roleDescription;
    private Integer hourlySalary;
    private Double monthlySalary;
    private Integer annualSalary;

    public Employee(Integer id, String name, String contractTypeName, Integer roleId, String roleName, String roleDescription, Integer hourlySalary, Double monthlySalary) {
        this.id = id;
        this.name = name;
        this.contractTypeName = contractTypeName;
        this.roleId = roleId;
        this.roleName = roleName;
        this.roleDescription = roleDescription;
        this.hourlySalary = hourlySalary;
        this.monthlySalary = monthlySalary;
        this.annualSalary = hourlySalary * AGE_VALUE * VALUE;
    }
}
