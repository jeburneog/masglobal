import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Employee } from 'src/app/model/Employee';
import { EmployeeService } from 'src/app/services/emeployee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  dataSource: MatTableDataSource<Employee> | undefined;

  isShown: boolean = false ;

  displayedColumns = ['id', 'name', 'contractTypeName', 'roleId', 'roleName', 'hourlySalary', 'monthlySalary', 'annualSalary'];
  
  constructor(private employeeService: EmployeeService,
              ) { }

  ngOnInit(): void {
  }

  getData(id?: any) {
    if(id) {
      this.getEmployeeById(id);
    } else {
      this.getEmployee();
    }
  }

  private getEmployee() {
    this.employeeService.listEmployee().subscribe(data => {
      this.isShown = true;
      this.dataSource = new MatTableDataSource(data);
    })
  }

  private getEmployeeById(id: number) {
    this.employeeService.listEmployeeByID(id).subscribe(data => {
      this.isShown = true;
      this.dataSource = new MatTableDataSource(data);
    })
  }

  getHome() {
    window.location.reload();
  }

}
