import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../model/Employee';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private url: string = `${environment.HOST}/api`;

  constructor(private http: HttpClient) { }

  
  listEmployee(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.url);
  }

  listEmployeeByID(id: number): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.url}/${id}`);
  }

}
