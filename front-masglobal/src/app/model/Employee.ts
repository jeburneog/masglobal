export class Employee {
    id: number;
    name: string;
    contractTypeName: string;
    roleId: number;
    roleName: string;
    roleDescription: string;
    hourlySalary: number;
    monthlySalary: number;
    annualSalary: number;

    constructor( id: number, name: string, contractTypeName: string, roleId: number, roleName: string, roleDescription: string, hourlySalary: number, monthlySalary: number, annualSalary: number) {
        this.id = id;
        this.name = name;
        this.contractTypeName = contractTypeName;
        this.roleId = roleId;
        this.roleName = roleName;
        this.roleDescription = roleDescription;
        this.hourlySalary = hourlySalary;
        this.monthlySalary = monthlySalary;
        this.annualSalary = annualSalary;
    }
}

